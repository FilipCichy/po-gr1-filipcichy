package pl.edu.uwm.wmii.filipcichy.laboratorium00.cwi0;

public class cwi0 {

    public static void main(String[] args){
        // 31 + 29 + 31
        System.out.println("1) " + (31 + 29 + 31));
        System.out.println("2) " + (1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10));
        System.out.println("3) " + (1 * 2 * 3 * 4 * 5 * 6 * 7 * 8 * 9 * 10));
        System.out.println("4)" + (1000 * 1.06) + " " + ((1000 * 1.06) * 1.06) + " " + (((1000 * 1.06) * 1.06) * 1.06));
        System.out.println("5)\n--------\n| JAVA |\n--------");
        System.out.println("6)\n +\"\"\"\"\"+\n0| o o |0\n |  .. |\n |  v  |\n _______");
        System.out.println("7)\n* * * * *     *             *             *             *  *\n*             *             *             *             *     *\n*             *             *             *             *     *\n* * * * *     *             *             *             *  *\n*             *             *             *             *\n*             *             *             *             *\n*             *             * * * * *     *             *");
        System.out.println("8)\n   +   \n  + +  \n +   + \n+-----+\n| .-. |\n| | | |\n++-+-+-+");
        System.out.println("9)\n __v_\n(____\\/{");
        System.out.println("10)\nDomainika\nMichal\nOla");
        System.out.println("11)\nEviva l'arte! Człowiek zginąć musi —\n" +
                "Cóż, kto pieniędzy nie ma, jest pariasem,\n" +
                "Nędza porywa za gardło i dusi —\n" +
                "Zginąć, to zginąć, jak pies, a tymczasem,\n" +
                "Choć życie nasze splunięcia nie warte:\n" +
                "Eviva l'arte!\n" +
                "Eviva l'arte! Niechaj pasie brzuchy\n" +
                "Nędzny filistrów naród! My artyści,\n" +
                "My, którym często na chleb braknie suchy, \n" +
                "My, do jesiennych tak podobni liści,\n" +
                "I tak wykrzykniem: gdy wszystko nic warte,\n" +
                "Eviva l'arte!\n" +
                "Eviva l'arte! Duma naszym bogiem,\n" +
                "Sława nam słońcem, nam, królom bez ziemi!\n" +
                "Możemy z głodu skonać gdzieś pod progiem,\n" +
                "Ale jak orły z skrzydłyzłamanemi —\n" +
                "Więc naprzód! Cóż jest prócz sławy co warte?\n" +
                "Eviva l'arte!\n" +
                "Eviva l'arte! W piersiach naszych płoną\n" +
                "Ognie przez Boga samego włożone:\n" +
                "Więc patrzym na tłum z głową podniesioną,\n" +
                "Laurów za złotą nie damy koronę, \n" +
                "I chociaż życie nasze nic nie warte:\n" +
                "Eviva l'arte!");
        System.out.println("12)\n  |* * * * * * * * * * OOOOOOOOOOOOOOOOOOOOOOOOO|\n" +
                "  | * * * * * * * * *  :::::::::::::::::::::::::|\n" +
                "  |* * * * * * * * * * OOOOOOOOOOOOOOOOOOOOOOOOO|\n" +
                "  | * * * * * * * * *  :::::::::::::::::::::::::|\n" +
                "  |* * * * * * * * * * OOOOOOOOOOOOOOOOOOOOOOOOO|\n" +
                "  | * * * * * * * * *  ::::::::::::::::::::;::::|\n" +
                "  |* * * * * * * * * * OOOOOOOOOOOOOOOOOOOOOOOOO|\n" +
                "  |:::::::::::::::::::::::::::::::::::::::::::::|\n" +
                "  |OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO|\n" +
                "  |:::::::::::::::::::::::::::::::::::::::::::::|\n" +
                "  |OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO|\n" +
                "  |:::::::::::::::::::::::::::::::::::::::::::::|\n" +
                "  |OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO|\n" +
                "\n" +
                "------------------------------------------------");


    }

}
