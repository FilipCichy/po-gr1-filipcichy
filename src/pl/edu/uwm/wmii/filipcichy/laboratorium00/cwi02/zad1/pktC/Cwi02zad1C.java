package pl.edu.uwm.wmii.filipcichy.laboratorium00.cwi02.zad1.pktC;

import java.util.Random;

public class Cwi02zad1C {

    public static void main(String[] args){
        int n = 71;
        int max = Integer.MIN_VALUE, ile = 0;

        int[] tab = new int[n];
        Random r = new Random();
        for(int i = 0; i < tab.length; i++){
            tab[i] = r.nextInt(1000) * (int) Math.pow(-1, r.nextInt(10));
            if(tab[i] > max){
                max = tab[i];
                ile = 1;
            }
            else if(tab[i] == max)
                ile++;
        }
        System.out.println("najwieksza liczba to: "+max+" i wystepuje "+ile+" razy");
    }
}
