package pl.edu.uwm.wmii.filipcichy.laboratorium00.cwi02.zad1.pktD;

import java.util.Random;

public class Cwi02zad1D {

    public static void main(String[] args){
        int n = 71;
        int dodanie = 0, ujemne = 0;

        int[] tab = new int[n];
        Random r = new Random();
        for(int i = 0; i < tab.length; i++){
            tab[i] = r.nextInt(1000) * (int) Math.pow(-1, r.nextInt(10));
            if(tab[i] > 0){
                dodanie += tab[i];
            }
            else if(tab[i] < 0)
                ujemne += tab[i];
        }
        System.out.println("suma liczb dodatnich to: "+dodanie+", ujemnych: "+ujemne);
    }
}
