package pl.edu.uwm.wmii.filipcichy.laboratorium00.cwi02.zad1.pktE;

import java.util.Random;

public class Cwi02zad1E {

    public static void main(String[] args){
        int n = 71;
        int max = 0, ile = 0;

        int[] tab = new int[n];
        Random r = new Random();
        for(int i = 0; i < tab.length; i++){
            tab[i] = r.nextInt(1000) * (int) Math.pow(-1, r.nextInt(10));
            if(tab[i] > 0){
                ile++;
            }
            else if(tab[i] < 0)
                ile = 0;
            if(ile > max)
                max++;
        }
        System.out.println("najdluzszy ciag liczb dodatnich to "+max+" liczb");
    }
}
