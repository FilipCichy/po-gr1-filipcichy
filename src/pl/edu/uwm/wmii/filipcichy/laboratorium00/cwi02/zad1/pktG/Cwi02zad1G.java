package pl.edu.uwm.wmii.filipcichy.laboratorium00.cwi02.zad1.pktG;

import java.util.Random;
import java.util.Scanner;

public class Cwi02zad1G {

    public static void main(String[] args){
        int n = 10;
        int max = 0, ile = 0;

        int[] tab = new int[n];
        Random r = new Random();
        for(int i = 0; i < tab.length; i++){
            tab[i] = r.nextInt(1000) * (int) Math.pow(-1, r.nextInt(10));
        }
        System.out.println("podaj lewy przedzial: ");
        int lewy = new Scanner(System.in).nextInt();

        System.out.println("podaj prawy przedzial: ");
        int prawy = new Scanner(System.in).nextInt();

        if(lewy >= prawy){
            System.err.println("zly przedzial");
            return;
        }

        int[] tab2 = new int[prawy-lewy+1];
        int tab3[] = tab;
        int k = 0;
        for (int i = 0; i < tab.length; i++){
            if(i >= lewy && i <= prawy && k <= i){
                tab2[k] = tab[i];
                k++;
            }
        }
        k = prawy - lewy;
        for (int i = 0; i < tab.length; i++){
            if(i >= lewy && i <= prawy && k <= i){
                tab[i] = tab2[k];
                k--;
            }
            System.out.println(tab[i]+"   "+tab3[i]);
        }
    }
}
