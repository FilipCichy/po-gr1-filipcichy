package pl.edu.uwm.wmii.filipcichy.laboratorium00.cwi04;

import java.util.Arrays;

public class zad1 {

    public static void main(String[] args){
        String string = "Nie do konca dlugi napis ale napis";
        char znak = 'n';
        System.out.println("znak " + znak + " wystepuje w napisie: " + string + " " + countChar(string, znak) + " razy");

        String string2 = "napis";
        System.out.println("napis: " + string2 + " wysttepuje w napisie: " + string + " " + countSubStr(string,string2) + " razy");
        System.out.println(middle("mid"));
        System.out.println(repeat(string2, 5));
        for(int i: where(string, string2))
            System.out.print(i + " ");
        System.out.println("\n"+change("MALY duzy"));
        System.out.println(nice("12345612345"));
        System.out.println(nice("|", 4, "123456123451234561234512345612345"));
    }


    public static int countChar(String str,char c){
        int x = 0;
        for(char z : str.toCharArray()){
            if(z == c) x++;
        }
        return x;
    }

    public static int countSubStr(String str, String subStr){
        int x = 0;
        for (int i = 0; i < str.length(); i++){
            if(str.charAt(i) == subStr.charAt(0)){
                if(str.substring(i, i + subStr.length()).equals(subStr)){
                    x++;
                }
            }
        }
        return x;
    }

    private static String middle(String str){
        int mid = str.length()/2;
        if(str.length() % 2 != 0) return str.substring(mid, mid + 1);
        return str.substring(mid - 1, mid + 1);
    }

    private static String repeat(String str,int n){
        return  str.repeat(n);
    }

    private static int[] where(String str, String subStr){

        int[] tab = new int[countSubStr(str,subStr)];
        int x = 0;

        for (int i = 0; i < str.length(); i++){
            if(str.charAt(i) == subStr.charAt(0)){
                if(str.substring(i, i + subStr.length()).equals(subStr)){
                    tab[x] = i;
                    x++;
                }
            }
        }
        return tab;
    }
    
    private static String change(String str){
        StringBuffer s = new StringBuffer();
        for (char c: str.toCharArray()){
            if(Character.isLowerCase(c))
                s.append(Character.toUpperCase(c));
            else
                s.append(Character.toLowerCase(c));
        }
        return s.toString();
    }

    private static String nice(String str){
        StringBuffer s = new StringBuffer();
        s.append(str);
        int count = 0;
        for(int i = str.length() - 1; i > 0; i--){
            if(Character.isDigit(str.charAt(i))){
                count++;
            }
            if(count == 3){
                s.insert(i, "\"");
                count = 0;
            }
        }
        return s.toString();
    }

    private static String nice(String znak, int liczba, String str){
        StringBuffer s = new StringBuffer();
        s.append(str);
        int count = 0;
        for(int i = str.length() - 1; i > 0; i--){
            if(Character.isDigit(str.charAt(i))){
                count++;
            }
            if(count == liczba){
                s.insert(i, znak);
                count = 0;
            }
        }
        return s.toString();
    }

}
