package pl.edu.uwm.wmii.filipcichy.laboratorium00.cwi04;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class zad2 {

    public static void main(String[] args){
        String path = "zad.txt";
        System.out.println(finder(path, 'c'));
    }

    private static int finder(String path, char znak){
        File file = new File(path);
        int count = 0;
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()){
                count += zad1.countChar(scanner.nextLine(), znak);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return count;
    }

}
