package pl.edu.uwm.wmii.filipcichy.laboratorium00.cwi04;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class zad3 {

    public static void main(String[] args){
        String path = "zad.txt";
        System.out.println(finder(path, "qw"));
    }

    private static int finder(String path, String subStr){
        File file = new File(path);
        int count = 0;
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()){
                count += zad1.countSubStr(scanner.nextLine(), subStr);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return count;
    }

}
