package pl.edu.uwm.wmii.filipcichy.laboratorium00.cwi04;

import java.math.BigDecimal;

public class zad4 {

    public static void main(String[] args){
        int n = 64;
        ile(n);
    }

    private static void ile(int n){
        BigDecimal x = new BigDecimal(1);
        for (int i = 0; i < n * n; i++){
            x = x.multiply(new BigDecimal(2));
        }
        System.out.println(x.toString());
    }

}
