package pl.edu.uwm.wmii.filipcichy.laboratorium00.cwi04;

import java.math.BigDecimal;
import java.math.MathContext;

public class zad5 {

    public static void main(String[] args) {
        int n = 15;
        int k = 5000;
        double p = 1.07;
        ile(n, k, p);
    }

    private static void ile(int n, int k, double p){
        BigDecimal x = new BigDecimal(k);
        for(int i = 0; i < n; i++)
            x = x.multiply(new BigDecimal(p));
        System.out.println(x.setScale(2, BigDecimal.ROUND_UP));
    }

}
