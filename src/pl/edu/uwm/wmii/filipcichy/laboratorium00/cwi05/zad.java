package pl.edu.uwm.wmii.filipcichy.laboratorium00.cwi05;

import java.util.ArrayList;

public class zad {

    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<>();
        ArrayList<Integer> b = new ArrayList<>();
        a.add(1); a.add(4); a.add(9); a.add(16);
        b.add(9); b.add(7); b.add(4); b.add(9); b.add(11);
        /* zad1 */
        for(int i: append(a, b))
            System.out.print(i + " ");

        System.out.println();

        /* zad2 */
        for(int i: merge(a, b))
            System.out.print(i + " ");

        System.out.println();

        /* zad3 */
        for(int i: mergeSorted(a, b))
            System.out.print(i + " ");

        System.out.println();

        /* zad4 */
        for(int i: reversed(a))
            System.out.print(i + " ");

        System.out.println();

        /* zad5 */
        reverse(a);

    }

    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b){
        a.addAll(b);
        return a;
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> c = new ArrayList<>();
        int length;
        if(a.size() > b.size()) length = a.size();
        else length = b.size();
        for (int i = 0; i < length; i++){
            if(i < a.size())
                c.add(a.get(i));
            if(i < b.size())
                c.add(b.get(i));
        }
        return c;
    }

    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b){
        a.addAll(b);
        a.sort(Integer::compareTo);
        return a;
    }

    public static  ArrayList<Integer> reversed(ArrayList<Integer> a){
        ArrayList<Integer> b = new ArrayList<>();
        for(int i = a.size() - 1; i >= 0; i--){
            b.add(a.get(i));
        }
        return b;
    }

    public static void reverse(ArrayList<Integer> a){
        ArrayList<Integer> b = new ArrayList<>();
        for(int i = a.size() - 1; i >= 0; i--){
            b.add(a.get(i));
            System.out.print(a.get(i));
        }
    }

}
