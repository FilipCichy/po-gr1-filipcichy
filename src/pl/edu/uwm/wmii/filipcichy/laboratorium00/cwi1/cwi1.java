package pl.edu.uwm.wmii.filipcichy.laboratorium00.cwi1;

import com.sun.tools.javac.Main;

import java.util.Scanner;

public class cwi1 {
    public static void main(String[] args){
        zad1();
        //zad2();
    }

    private static void zad1(){

        /* 1  */
        float wynik = 0;
        int n;
        Scanner scanner = new Scanner(System.in);

        System.out.println("a)");
        n = Math.abs(scanner.nextInt());
        for(int i = 1; i <= n; i++){
            wynik += i;
            System.out.print((int)wynik+" ");
        }

        wynik = 1;
        System.out.println("\nb)");
        n = Math.abs(scanner.nextInt());
        for(int i = 1; i <= n; i++){
            wynik *= i;
            System.out.print((int)wynik+" ");
        }

        wynik = 0;
        System.out.println("\nc)");
        n = Math.abs(scanner.nextInt());
        for(int i = 1; i <= n; i++){
            wynik += Math.abs(i);
            System.out.print((int)wynik+" ");
        }

        wynik = 0;
        System.out.println("\nd)");
        n = Math.abs(scanner.nextInt());
        for(int i = 1; i <= n; i++){
            wynik += Math.abs(Math.sqrt(i));
            System.out.print((int)wynik+" ");
        }

        wynik = 1;
        System.out.println("\ne)");
        n = Math.abs(scanner.nextInt());
        for(int i = 1; i <= n; i++){
            wynik *= Math.abs(i);
            System.out.print((int)wynik+" ");
        }

        wynik = 0;
        System.out.println("\nf)");
        n = Math.abs(scanner.nextInt());
        for(int i = 1; i <= n; i++){
            wynik += 2/i;
            System.out.print((int)wynik+" ");
        }

        wynik = 0;
        System.out.println("\nf)");
        n = Math.abs(scanner.nextInt());
        for(int i = 1; i <= n; i++){
            wynik += 2/i;
            System.out.print(wynik+" ");
        }

        System.out.println("\ng)");
        n = Math.abs(scanner.nextInt());
        for(int i = 1; i <= n; i++){
            wynik += i;
            System.out.print((int)wynik+" ");
        }

        wynik = 1;
        n = Math.abs(scanner.nextInt());
        for(int i = 1; i <= n; i++){
            wynik *= i;
            System.out.print((int)wynik+" ");
        }

        wynik = 0;
        System.out.println("\nh)");
        n = Math.abs(scanner.nextInt());
        for(int i = 1; i <= n; i++){
            wynik += Math.pow(-1,i + 1) * i;
            System.out.print((int)wynik+" ");
        }

        wynik = 0;
        System.out.println("\ni)");
        n = Math.abs(scanner.nextInt());
        for(int i = 1; i <= n; i++){
            wynik += (Math.pow(-1, i) * i)/factional(i);
            System.out.print(wynik+" ");
        }

    }

    private static int factional(int n){
        int wynik = 1;
        for(int i = 2; i<=n; i++)
            wynik *= i;

        return wynik;
    }

    private static void zad2(){
        Scanner scanner = new Scanner(System.in);
        int wynikA = 0;
        int wynikB = 0;
        int wynikC = 0;
        int wynikD = 0;
        int wynikE = 0;
        int wynikF = 0;
        int wynikG = 0;
        int wynikH = 0;
        int n;

        System.out.println("prosze podac liczbe: ");
        n = Math.abs(scanner.nextInt());
        double a = 1, a2 = 1;
        for (int i = 1; i <= n; i++){
            a = a + Math.pow(-4,i);
            if(a % 2 == 0) wynikA++;
            if(a % 3 == 0 && a % 5 != 0) wynikB++;
            if(Math.sqrt(a) % 2 == 0) wynikC++;
            if(a < ((a2) + (a + Math.pow(-4, i + 1)) / 2) && i > 1 && i < n) wynikD++;
            if(Math.pow(2,i) < a && a < factional(i) && 1 <= i && i <= n) wynikE++;
            if(paz(i) && a % 2 != 0) wynikF++;
            if(i > 0 && i % 2 != 0) wynikG++;
            if(Math.abs(a) < i * i) wynikH++;

            a2 = a;
            System.out.print(a + " ");
        }
        System.out.println("\na) jest " + wynikA + "liczb naturalnych");
        System.out.println("b) jest " + wynikB + "liczb podzielnych przez 3 i niepodzielnych przez 5");
        System.out.println("c) warunek spelnia " + wynikC + "liczb");
        System.out.println("d) warunek spelnia " + wynikD + "liczb");
        System.out.println("e) warunek spelnia " + wynikE + "liczb");
        System.out.println("f) warunek spelnia " + wynikF + "liczb");
        System.out.println("g) warunek spelnia " + wynikG + "liczb");
        System.out.println("h) warunek spelnia " + wynikH + "liczb");

        /* 2 */

        n = scanner.nextInt();
        int dodatnie = 0, ujemne = 0, zera = 0;
        for(int i = 0; i < n; i++){
            int x = scanner.nextInt();
            if(x > 0 )dodatnie++;
            else if(x < 0)ujemne++;
            else zera++;
        }
        System.out.println("jest "+dodatnie+"liczb dodatnich, "+ujemne+" ujemnych oraz "+zera+"zer");

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < n; i++){
            int x = scanner.nextInt();
            if(x < min) min = x;
            if(x > max) max = x;
        }
        System.out.println("max: "+max+" min: "+min);

    }

    private static boolean paz(int x){
        while (x > 10){
            if((x % 10) % 2 != 0) return true;
            x /= 10;
        }
        return false;
    }
}
